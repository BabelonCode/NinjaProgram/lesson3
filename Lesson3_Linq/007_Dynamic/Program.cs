﻿using System.Collections.Generic;

namespace _007_Dynamic
{
    class Program
    {
        static void Main(string[] args)
        {
            short[] arr = { 10, 20, 1000, 40 };
            var list = new List<int> { 10, 20, 1000, 40 };

            long sum1 = MyMath.Sum(arr);
            long sum2 = MyMath.Sum(list);
        }
    }
}
