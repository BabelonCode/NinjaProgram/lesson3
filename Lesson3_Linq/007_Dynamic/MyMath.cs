﻿using System;
using System.Collections;

namespace _007_Dynamic
{
    static class MyMath
    {
        //public static long Sum(IEnumerable source)
        //{
        //    long sum = 0;
        //    foreach (int item in source)
        //        sum += item;
        //    return sum;
        //}

        public static long Sum(dynamic source)
        {
            long sum = 0;
            foreach (int item in source)
                sum += item;
            return sum;
        }
    }
}
