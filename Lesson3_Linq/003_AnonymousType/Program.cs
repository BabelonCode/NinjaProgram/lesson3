﻿using System;

namespace _003_AnonymousType
{
    //https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/anonymous-types
    class Program
    {
        static void Main(string[] args)
        {
            string Name = "Arianna";
            DateTime dt = DateTime.Now;

            var o1 = new { Name, dt.Year };
            var o2 = new { Name, dt.Year };

            if(o1.Equals(o2))
            {

            }
        }
    }
}
