﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _005_ExpandoObject
{
    class Program
    {
        static void Main(string[] args)
        {
            dynamic e = new System.Dynamic.ExpandoObject();
            e.Name = "A1";
            e.Age = 18;

            Type t = e.GetType();
        }
    }
}
