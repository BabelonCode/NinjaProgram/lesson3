﻿using System;

namespace _004_AnonymousType
{
    //https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/anonymous-types
    class Program
    {
        static void Main(string[] args)
        {
            string Name = "A1";
            DateTime dt = DateTime.Now;

            var o1 = new { Name, dt.Year };

            var people = new[]
            {
                o1,
                new { Name= "A2", Year = 1990 },
                new { Name= "A3", Year = 1985 },
                new { Name= "A4", Year = 1986 },
            };
        }
    }
}
