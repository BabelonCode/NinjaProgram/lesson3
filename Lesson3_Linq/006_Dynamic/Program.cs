﻿using System;

namespace _006_Dynamic
{
    //https://docs.microsoft.com/en-us/dotnet/api/system.dynamic.dynamicobject?view=netframework-4.8
    class Program
    {
        static void Main(string[] args)
        {
            dynamic variable = 10;
            Console.WriteLine(variable);
            Console.WriteLine(variable.GetType());
            Console.WriteLine();

            variable = "Hello Word";
            Console.WriteLine(variable);
            Console.WriteLine(variable.GetType());
            Console.WriteLine();

            variable = DateTime.Now;
            Console.WriteLine(variable);
            Console.WriteLine(variable.GetType());
            Console.WriteLine();

            Console.ReadLine();
        }
    }
}
